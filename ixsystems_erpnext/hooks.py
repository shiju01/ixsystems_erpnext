# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from . import __version__ as app_version

app_name = "ixsystems_erpnext"
app_title = "Ixsystems Erpnext"
app_publisher = "MN Technique"
app_description = "ERPNext Customization for IX Systems"
app_icon = "fa fa-server"
app_color = "#489FDC"
app_email = "support@mntechnique.com"
app_license = "MIT"

# Includes in <head>
# ------------------

# include js, css files in header of desk.html
# app_include_css = "/assets/ixsystems_erpnext/css/ixsystems_erpnext.css"
# app_include_js = "/assets/ixsystems_erpnext/js/ixsystems_erpnext.js"

# include js, css files in header of web template
# web_include_css = "/assets/ixsystems_erpnext/css/ixsystems_erpnext.css"
# web_include_js = "/assets/ixsystems_erpnext/js/ixsystems_erpnext.js"

# Home Pages
# ----------

# application home page (will override Website Settings)
# home_page = "login"

# website user home page (by Role)
# role_home_page = {
#	"Role": "home_page"
# }

# Website user home page (by function)
# get_website_user_home_page = "ixsystems_erpnext.utils.get_home_page"

# Generators
# ----------

# automatically create page for each record of this doctype
# website_generators = ["Web Page"]

# Installation
# ------------

# before_install = "ixsystems_erpnext.install.before_install"
# after_install = "ixsystems_erpnext.install.after_install"

# Desk Notifications
# ------------------
# See frappe.core.notifications.get_notification_config

# notification_config = "ixsystems_erpnext.notifications.get_notification_config"

# Permissions
# -----------
# Permissions evaluated in scripted ways

# permission_query_conditions = {
# 	"Event": "frappe.desk.doctype.event.event.get_permission_query_conditions",
# }
#
# has_permission = {
# 	"Event": "frappe.desk.doctype.event.event.has_permission",
# }

# Document Events
# ---------------
# Hook on document methods and events

# doc_events = {
# 	"*": {
# 		"on_update": "method",
# 		"on_cancel": "method",
# 		"on_trash": "method"
#	}
# }

# Scheduled Tasks
# ---------------

# scheduler_events = {
# 	"all": [
# 		"ixsystems_erpnext.tasks.all"
# 	],
# 	"daily": [
# 		"ixsystems_erpnext.tasks.daily"
# 	],
# 	"hourly": [
# 		"ixsystems_erpnext.tasks.hourly"
# 	],
# 	"weekly": [
# 		"ixsystems_erpnext.tasks.weekly"
# 	]
# 	"monthly": [
# 		"ixsystems_erpnext.tasks.monthly"
# 	]
# }

# Testing
# -------

# before_tests = "ixsystems_erpnext.install.before_tests"

# Overriding Whitelisted Methods
# ------------------------------
#
# override_whitelisted_methods = {
# 	"frappe.desk.doctype.event.event.get_events": "ixsystems_erpnext.event.get_events"
# }

fixtures = [ 
	{	
		"dt":"Custom Field",
		"filters":[["name", "in", ["Account-ix_ac_no"]]]
	},
	{
		"dt":"Custom Script",
		"filters":[["name", "in", ["Account-Client"]]]
	}
]
