# -*- coding: utf-8 -*-
# Copyright (c) 2015, MN Technique and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class IXSettings(Document):
	def on_save(self):
		frappe.db.set_value("Account", "Source of Funds (Liabilities)", "ix_ac_no", self.ac_no_liabilities)
		frappe.db.set_value("Account", "Application of Funds (Assets)", "ix_ac_no", self.ac_no_assets)
		frappe.db.set_value("Account", "Equity", "ix_ac_no", self.ac_no_equity)
		frappe.db.set_value("Account", "Income", "ix_ac_no", self.ac_no_income)
		frappe.db.set_value("Account", "Expenses", "ix_ac_no", self.ac_no_expenses)

		frappe.db.commit()
